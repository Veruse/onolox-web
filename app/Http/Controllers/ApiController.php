<?php

namespace App\Http\Controllers;

use App\Discord\Bot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public static function createUser($discord_id)
    {
        return DB::table('lox')->insert([
            ['discord_id' => $discord_id]
        ]);
    }

    public static function userInfo($discord_id)
    {
        return Bot::userInfo($discord_id);
    }

    public static function loxAdd($discord_id, $amount)
    {
        $current = DB::table('lox')->select('lox')->where('discord_id', $discord_id)->first();
        //dd($current);
        return DB::table('lox')->where('discord_id', $discord_id)->update(['lox' => $current->lox + $amount]);
    }

    public static function loxRemove($discord_id, $amount)
    {
        $current = DB::table('lox')->select('lox')->where('discord_id', $discord_id)->first();
        //dd($current);
        return DB::table('lox')->where('discord_id', $discord_id)->update(['lox' => $current->lox - $amount]);
    }

    public function __construct()
    {
        $key = \request('key');
        if ($key !== "1234") {
            return abort(404);
        }
    }
}
