<?php

namespace App\Http\Controllers;

use App\Discord\Bot;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    public function sendMessage()
    {
        $channel = \request('channel');
        $message = \request('content');
        $data = Bot::sendMessage($channel, $message);
        return redirect()->route('dashboard');
    }
}
