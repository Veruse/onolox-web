<?php

namespace App\Http\Controllers;

use App\Discord\Bot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthenticationController extends Controller
{
    public function dashboard()
    {
            $data = json_decode(Bot::channels(), true);
            $loxDB = DB::table('lox')->select('*')->get();
            return view('dashboard', [
                'data' => $data,
                'loxDB' => $loxDB
            ]);
    }
}
