<?php

namespace App\Discord;


class Bot
{
    static function channels()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://discord.com/api/v6/guilds/742288550514524181/channels');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET'); // The request should be something like "PUT" or "DELETE"


        $headers = array();
        $headers[] = 'Authorization: Bot NzQyNjgyOTU1MjUxNzEyMDAx.XzJrZg.Qh3YheBSb-xlW6-vRq9Np-83b7s';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }

    static function userInfo($discord_id)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://discord.com/api/v6/users/'.$discord_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET'); // The request should be something like "PUT" or "DELETE"


        $headers = array();
        $headers[] = 'Authorization: Bot NzQyNjgyOTU1MjUxNzEyMDAx.XzJrZg.Qh3YheBSb-xlW6-vRq9Np-83b7s';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return json_decode($result, true);
    }

    static function sendMessage($channel, $message)
    {

        $data = array("content" => $message);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://discord.com/api/v6/channels/'.$channel.'/messages');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST'); // The request should be something like "PUT" or "DELETE"
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $headers = array();
        $headers[] = 'Authorization: Bot NzQyNjgyOTU1MjUxNzEyMDAx.XzJrZg.Qh3YheBSb-xlW6-vRq9Np-83b7s';
        $headers[] = 'Content-type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }
}
