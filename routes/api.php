<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('lox/createUser/{discord_id}', [ApiController::class, 'createUser'])->name('APICreateUser');
Route::get('lox/add/{discord_id}/{amount}', [ApiController::class, 'loxAdd'])->name('APIAddLox');
Route::get('lox/remove/{discord_id}/{amount}', [ApiController::class, 'loxRemove'])->name('APIRemoveLox');
Route::get('discord/userInfo/{discord_id}', [ApiController::class, 'userInfo']);
