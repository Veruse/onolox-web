@extends('template/auth')

@section('title', 'Login')

@section('content')
    <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            -ms-flex-align: center;
            -ms-flex-pack: center;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-pack: center;
            justify-content: center;
            padding-top: 40px;
            padding-bottom: 40px;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .checkbox {
            font-weight: 400;
        }
        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>
    <div class="container mt-5 text-center">
        <form class="form-signin" method="GET" action="{{ route('dashboard') }}">
            <img class="mb-4 rounded" src="https://i.veruse.dev/Dm0m8NwA.gif" alt="" width="72" height="72">
            @csrf

            <h1 class="h3 mb-3 font-weight-normal">Onolox Server Controller</h1>
            <input type="password" name="auth_code" id="inputPassword" class="form-control text-center" placeholder="Access Code" required="">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <p class="mt-5 mb-3 text-muted">© Onolox {{ date('Y') }}</p>
        </form>
    </div>
@endsection
