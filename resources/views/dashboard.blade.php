@extends('template/auth')

@section('title', 'Dashboard')

@section('content')
    <div class="container mt-5">
        <div class="container p-3" style="background-color: darkslategrey;border-left: 4px solid deepskyblue;border-radius: 5px;">
            <form method="post" action="{{ route('sendMessage') }}">
                @csrf

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Select a Channel</label>
                    <select class="form-control" name="channel" id="exampleFormControlSelect1">
                        @foreach($data as $channel)
                            @if($channel['type'] != 4)
                                @if($channel['type'] != 2)
                                    <option value="{{ $channel['id'] }}">{{ $channel['name'] }}</option>
                                @endif
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Message</label>
                    <textarea class="form-control" name="content" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" style="width: 100%;" value="Send Message">
                </div>
            </form>
        </div>
    </div>
    <br><br>
    <div class="container mt-5">
        <div class="container p-3" style="background-color: darkslategrey;border-left: 4px solid deepskyblue;border-radius: 5px;">
            <table class="table text-white">
                <thead class="text-center">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Discord</th>
                    <th scope="col">Lox</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @php($loxSideCounter = 1)
                @foreach($loxDB as $lox)
                <tr class="text-center">
                    <th scope="row">{{ $loxSideCounter }}</th>
                    <td data-toggle="popover" title="{{ \App\Discord\Bot::userInfo($lox->discord_id)['username'] }}'s ID" data-placement="top" data-content="ID: {{ $lox->discord_id }}">{{ \App\Discord\Bot::userInfo($lox->discord_id)['username'] }}#{{ \App\Discord\Bot::userInfo($lox->discord_id)['discriminator'] }}</td>
                    <td>{{ $lox->lox }}</td>
                    <td><a href="" class="btn btn-primary">Add</a> <a href="" class="btn btn-secondary">Remove</a> <a href="" class="btn btn-danger">Reset Lox</a></td>
                </tr>
                    @php($loxSideCounter++)
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
